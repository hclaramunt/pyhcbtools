==================
pyhcbtools Package
==================

Main features
-------------

* SQL Server Database access
* Date/time manipulation

Installation
------------

The latest version can be installed directly from Bitbucket:

.. code-block:: bash

    $ pip install git+https://bitbucket.org/hclaramunt/pyhcbtools.git


.. code-block:: bash

    $ pip install https://bitbucket.org/hclaramunt/pyhcbtools/downloads/pyhcbtools.tar.gz

Author
------

* Hector Claramunt
