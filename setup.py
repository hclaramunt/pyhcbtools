# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='pyhcbtools',
      version='1.0.0',
      description='Tools for python scripts',
      long_description=readme(),
      url='https://bitbucket.org/hclaramunt/pyhcbtools',
      author='Hector Claramunt',
      author_email='hclaramunt@gmail.com',
      license='MIT',
      packages=find_packages(),
#      packages=['pyhcbtools'],
      install_requires=[
            'logging',
            'pyodbc',
            'datetime'
      ],
      zip_safe=False)