# -*- coding: utf-8 -*-

import logging
import pyodbc
from . import DatabaseConnector

log = logging.getLogger(__name__)


class SQLServerConnector(DatabaseConnector):
    def __init__(self, *args):
        DatabaseConnector.__init__(self, *args)
        self.db_conn_str = 'DSN=%s;UID=%s;PWD=%s;DATABASE=%s' % (self.db_dsn, self.db_uid, self.db_pwd, self.db_name)
        self.cursor = None
        self.commit_after_exec = True

    def connect(self):
        self.db_conn = pyodbc.connect(self.db_conn_str)
        self.cursor = self.db_conn.cursor()

    def close(self):
        if self.db_conn:
            self.db_conn.close()

    def fetch_all(self, *query):
        rows = []
        if self.execute(*query):
            rows = list(self.cursor.fetchall())
        return rows

    def fetch_one(self, *query):
        res = None
        if self.execute(*query):
            res = self.cursor.fetchone()
        return res

    def execute(self, *query_args, **kwargs):
        res = self._execute(*query_args)
        if 'commit' in kwargs and kwargs['commit']:
            self.commit()
        return res

    def _execute(self, *query_args):
        count = -1
        try:
            encoded_args = []
            for arg in query_args:
                if isinstance(arg, basestring):
                    arg = arg.encode('utf-8')
                encoded_args.append(arg)
                # log.debug(u"Executing DB sentence: {0}".format(u",".join([str(x) for x in encoded_args])))
                # for i, elem in enumerate(query_args):
                # log.debug(u"arg[{0}] = '{1}' ({2})".format(i, elem, type(elem)))
            count = self.cursor.execute(*encoded_args).rowcount
            log.debug("Finished DB sentence execution. Commit in course...")
        except Exception, e:
            log.error("Error executing db sentence: {0}".format(str(e)))
        return count

    def commit(self):
        self.db_conn.commit()
        log.debug("Commit finished")
