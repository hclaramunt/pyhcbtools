# -*- coding: utf-8 -*-

class DatabaseConnector(object):
    def __init__(self, db_dsn, db_name, db_uid, db_pwd):
        self.db_dsn = db_dsn
        self.db_uid = db_uid
        self.db_pwd = db_pwd
        self.db_name = db_name
        self.db_conn = None
        # Establish connection to the database

    def connect(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError

    def fetch_all(self, *query):
        raise NotImplementedError

    def fetch_one(self, *query):
        raise NotImplementedError

    def execute(self, *query):
        raise NotImplementedError
