# -*- coding: utf-8 -*-

from .sqlserver import SQLServerConnector


class DatabaseConnectorFactory (object):

    class dbtype:
        SQLSERVER = 'sqlserver'
        MYSQL = 'mysql'
        SQLITE = 'sqlite'
        PGSQL = 'pgsql'

    @classmethod
    def get_instance(cls, type, *args):
        db_init_func = cls.__get_funct_by_type(type)
        if not db_init_func:
            raise Exception('Database type not recognized. Type=[%s]' % type)
        return db_init_func(*args)

    @classmethod
    def __get_funct_by_type(cls, type):
        create_functs = {
            cls.dbtype.SQLSERVER: cls.__create_sqlserver_connector
        }
        return create_functs[type] if type in create_functs else None

    @staticmethod
    def __create_sqlserver_connector(*args):
        return SQLServerConnector(*args)
