# -*- coding: utf-8 -*-

from datetime import datetime


def datetime2timestamp(dt):
    epoch = datetime(1970, 1, 1)
    td = dt - epoch
    # return td.total_seconds()
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 1e6


def date2timestamp(d):
    dt = datetime(year=d.year, month=d.month, day=d.day)
    return datetime2timestamp(dt)
